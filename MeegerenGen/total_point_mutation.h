﻿#pragma once
#include "concave_polygon_mutation.h"

class total_point_mutation : public concave_polygon_mutation
{
	int mutation_chance_;
	
	std::pair<float, float> point_range_horizontal_;
	std::pair<float, float> point_range_vertical_;

public:
	total_point_mutation(int mutation_chance, std::pair<float, float> point_range_horizontal = { -1.f, 1.f }, std::pair<float, float> point_range_vertical = { -1.f, 1.f });

	bool mutate(concave_polygon_solution* sol) override;
};
