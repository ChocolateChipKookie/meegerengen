#pragma once
#include "solution.h"
#include <vector>

template<typename sol_t>
class evaluator
{
	static_assert(std::is_base_of<solution, sol_t>::value, "sol_t must derive from solution");

public:
	virtual ~evaluator() = default;

	virtual void evaluate(sol_t* sol) = 0;
	virtual void evaluate_generation(std::vector<sol_t*>& generation)
	{
		for (auto& sol : generation)
		{
			evaluate(sol);
		}
	}
};
