﻿#pragma once
#include "context.h"
#include <string>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

namespace meg
{

	class window_context final : public context
	{
	private:
		GLFWwindow* window_{ nullptr };
		static void framebuffer_size_callback(GLFWwindow* window, int width, int height);
		static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

		public:
		window_context(int width, int height, const std::string& name, bool alpha = true);
		~window_context();

		bool should_close() const;

		void set_process_input(void(*function)(GLFWwindow*, int, int, int, int)) const;
		void set_should_close(bool value) const;
		void swap_buffers() const;
		static void poll_events();
		void set_viewport() const;
		void resize(int width, int height) const;
		unsigned get_width() const;
		unsigned get_height() const;
		void use() override;
	};

	using window = window_context;

}
