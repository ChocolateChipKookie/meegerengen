﻿#pragma once
#include "selection.h"
#include <algorithm>
#include "random_engine.h"
#include <cassert>

template<typename sol_t>
class k_tournament_selection : public selection<sol_t>
{
	static_assert(std::is_base_of<solution, sol_t>::value, "sol_t must derive from solution");

	unsigned k_;
public:

	k_tournament_selection(unsigned k) : k_(k) {};

	sol_t* select(std::vector<sol_t*>& solutions) override;
	std::vector<sol_t*> select_n(std::vector<sol_t*>& solutions, unsigned count) override;
	void setK(unsigned k);
	unsigned getK() const;
};

template <typename sol_t>
sol_t* k_tournament_selection<sol_t>::select(std::vector<sol_t*>& solutions)
{
	std::vector<sol_t*> tmp_results;

	for (unsigned k = 0; k < k_; ++k)
	{
		sol_t* tmp_ptr;
		do
		{
			tmp_ptr = solutions[kki::random::rng.get_random(static_cast<int>(solutions.size()) - 1)];
		} while (
			std::find(tmp_results.begin(), tmp_results.end(), tmp_ptr) != tmp_results.end()
			);

		tmp_results.push_back(tmp_ptr);
	}

	return *std::max_element(tmp_results.begin(), tmp_results.end(), [](solution* s1, solution* s2) {return s1->fitness < s2->fitness; });
}

template <typename sol_t>
std::vector<sol_t*> k_tournament_selection<sol_t>::select_n(std::vector<sol_t*>& solutions, unsigned count)
{
	assert(count < solutions.size());
	std::vector<sol_t*> result;

	for (unsigned counter = 0; counter < count; ++counter)
	{
		std::vector<sol_t*> tmp_results;

		for (unsigned k = 0; k < k_; ++k)
		{
			sol_t* tmp_ptr;
			do
			{
				tmp_ptr = solutions[kki::random::rng.get_random(static_cast<int>(solutions.size()) - 1)];
			} while (
				std::find(tmp_results.begin(), tmp_results.end(), tmp_ptr) != tmp_results.end() ||
				std::find(result.begin(), result.end(), tmp_ptr) != result.end()
				);

			tmp_results.push_back(tmp_ptr);
		}

		sol_t* s = *std::max_element(tmp_results.begin(), tmp_results.end(), [](sol_t* s1, sol_t* s2) {return s1->fitness < s2->fitness; });
		result.push_back(s);
	}

	return result;
}

template <typename sol_t>
void k_tournament_selection<sol_t>::setK(unsigned k)
{
	this->k_ = k;
}

template <typename sol_t>
unsigned k_tournament_selection<sol_t>::getK() const
{
	return k_;
}
