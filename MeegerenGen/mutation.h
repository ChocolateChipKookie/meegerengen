#pragma once
#include <type_traits>
#include "solution.h"

template<typename sol_t>
class mutation
{
	static_assert(std::is_base_of<solution, sol_t>::value, "sol_t must be a have solution as base");

public:

	virtual ~mutation() = default;

	virtual bool mutate(sol_t* sol) = 0;
};
