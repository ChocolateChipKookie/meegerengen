﻿#pragma once
#include "concave_polygon_mutation.h"

class swap_polygon_mutation : public concave_polygon_mutation
{
private:
	int mutation_chance_;
public:
	explicit swap_polygon_mutation(int mutation_chance);

	bool mutate(concave_polygon_solution* sol) override;

};
