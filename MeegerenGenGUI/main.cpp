#include "meegerengengui.h"
#include <QtWidgets/QApplication>
#include <QtWidgets/QPushButton>
#include <iostream>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MeegerenGenGUI w;

	createOptionsGroupBox();
	createButtonBox();

	QGridLayout mainLayout;
	mainLayout->addWidget(rotatableGroupBox, 0, 0);
	mainLayout->addWidget(optionsGroupBox, 1, 0);
	mainLayout->addWidget(buttonBox, 2, 0);
	setLayout(mainLayout);

	mainLayout->setSizeConstraint(QLayout::SetMinimumSize);

	setWindowTitle(tr("Dynamic Layouts"));



	w.show();
	return a.exec();
}
