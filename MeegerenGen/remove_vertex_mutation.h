#pragma once
#include "concave_polygon_mutation.h"


class remove_vertex_mutation : public concave_polygon_mutation
{
private:
	int mutation_chance_;
public:
	remove_vertex_mutation(int mutation_chance);

	bool mutate(concave_polygon_solution* sol) override;
};

