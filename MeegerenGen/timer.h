#pragma once
#include <chrono>
#include <map>
#include <string>

class timer
{
public:
	class stopwatch
	{
	private:

		std::chrono::time_point<std::chrono::high_resolution_clock> start_time_;
		std::chrono::duration<double> total_{ 0 };

	public:
		void start()
		{
			start_time_ = std::chrono::high_resolution_clock::now();
		}

		void end()
		{

			total_ += std::chrono::high_resolution_clock::now() - start_time_;
		}

		void reset()
		{
			total_ = std::chrono::duration<double>{ 0 };
		}

		std::chrono::duration<double> get_total()
		{
			return total_;
		}

		std::chrono::duration<double> operator ()() const
		{
			return total_;
		}

		explicit operator std::string() const
		{
			return std::to_string(total_.count());
		}

		explicit operator double() const
		{
			return total_.count();
		}
	};

private:
	std::map<std::string, stopwatch> stopwatches_;

public:
	
	stopwatch& operator[](std::string& str)
	{
		return stopwatches_[str];
	}
	
	stopwatch& operator[](const char* str)
	{
		return stopwatches_[str];
	}

	void reset_all()
	{
		for(auto& sw : stopwatches_)
		{
			sw.second.reset();
		}
	}

	operator std::string()
	{
		std::string total;
		for(auto& sw : stopwatches_)
		{
			total += sw.first + ": " + std::to_string(sw.second().count()) + '\n';
		}
		return total;
	}

	static timer& global()
	{
		static timer t;
		return t;
	}
};

