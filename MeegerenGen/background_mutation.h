﻿#pragma once
#include "concave_polygon_mutation.h"

class background_mutation : public concave_polygon_mutation
{
	int mutation_chance_;
	bool colored_;

	int separation_factor_;

	float sigma_;

	std::pair<float, float> color_range_;

public:

	background_mutation(int mutation_chance, bool colored, double separation_factor, float sigma,
	                    const std::pair<float, float>& color_range);

	bool mutate(concave_polygon_solution* sol) override;
};
