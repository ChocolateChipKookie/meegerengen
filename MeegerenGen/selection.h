#pragma once
#include <vector>
#include <type_traits>
#include "solution.h"

template<typename sol_t>
class selection
{
	static_assert(std::is_base_of<solution, sol_t>::value, "sol_t must derive from solution");

public:
	virtual ~selection() = default;

	virtual sol_t* select(std::vector<sol_t*>& solutions) = 0;
	virtual std::vector<sol_t*> select_n(std::vector<sol_t*>& solutions, unsigned count)
	{
		std::vector<sol_t*> res;
		res.reserve(count);
		for(unsigned i = 0; i < count; ++i)
		{
			res.push_back(select(solutions));
		}
		return res;
	}
};
