#version 330 core
out vec4 FragColor;
  
in vec2 TexCoord;

uniform sampler2D texture1;
uniform sampler2D texture2;

void main()
{
   	FragColor.r = abs(texture(texture2, TexCoord).r - texture(texture1, TexCoord).r);
    FragColor.g = abs(texture(texture2, TexCoord).g - texture(texture1, TexCoord).g);
    FragColor.b = abs(texture(texture2, TexCoord).b - texture(texture1, TexCoord).b);
    FragColor.a = 1;
}