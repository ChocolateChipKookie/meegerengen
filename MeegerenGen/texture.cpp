﻿#include "texture.h"
#include <cassert>
#include <opencv2/opencv.hpp>
#include "graphics_error.h"
#include <glad/glad.h>

int testMaxMipmapLevel()
{
	int max_level;
	glGetTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, &max_level);
	int max_mipmap = -1;
	for (int i = 0; i < max_level; ++i)
	{
		int width;
		glGetTexLevelParameteriv(GL_TEXTURE_2D, i, GL_TEXTURE_WIDTH, &width);
		if (0 == width)
		{
			max_mipmap = i - 1;
			break;
		}
	}
	return max_mipmap;
}

meg::texture::texture(std::string filepath) : texture(cv::imread(filepath))
{
}

meg::texture::texture(cv::Mat image) : in_use_(true)
{
	glGenTextures(1, &texture_);
	glBindTexture(GL_TEXTURE_2D, texture_);

	//Set wrapping to repeat
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	//Set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	int w = (image.cols / 4) * 4;
	int h = (image.rows / 4) * 4;

	cv::resize(image, image, cv::Size(w, h));

	cv::flip(image, image, 0);

	width_ = image.cols;
	height_ = image.rows;

	cv::waitKey();
	if (!image.empty())
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width_, height_, 0, GL_BGR, GL_UNSIGNED_BYTE, image.data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		throw graphics_error{ "Failed to load texture" };
	}
	max_mipmap_level_ = testMaxMipmapLevel();
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);
}

meg::texture::texture(int width, int height) : width_(width), height_(height), in_use_(true)
{
	glGenTextures(1, &texture_);
	glBindTexture(GL_TEXTURE_2D, texture_);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
	max_mipmap_level_ = testMaxMipmapLevel();
	glBindTexture(GL_TEXTURE_2D, 0);
}

meg::texture::texture() : in_use_(false)
{
}

meg::texture::~texture()
{
	if (in_use_)
	{
		glDeleteTextures(1, &texture_);
	}
}

void meg::texture::generate_mip_map() const
{
	use();
	glGenerateMipmap(GL_TEXTURE_2D);
	end();
}

void meg::texture::use() const
{
	assert(in_use_);
	glBindTexture(GL_TEXTURE_2D, texture_);
}

void meg::texture::end() const
{
	assert(in_use_);
	glBindTexture(GL_TEXTURE_2D, 0);
}

unsigned meg::texture::get_id() const
{
	assert(in_use_);
	return texture_;
}

void meg::texture::clear()
{
	assert(in_use_);
	if(in_use_)
	{
		in_use_ = false;
		glDeleteTextures(1, &texture_);
	}
}

void meg::texture::create(int width, int height)
{
	assert(!in_use_);
	this->width_ = width;
	this->height_ = height;
	in_use_ = true;
	glGenTextures(1, &texture_);
	glBindTexture(GL_TEXTURE_2D, texture_);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
	max_mipmap_level_ = testMaxMipmapLevel();
	glBindTexture(GL_TEXTURE_2D, 0);
}

int meg::texture::get_width() const
{
	assert(in_use_);
	return width_;
}

int meg::texture::get_height() const
{
	assert(in_use_);
	return height_;
}

int meg::texture::get_max_mipmap_level() const
{
	assert(in_use_);
	return max_mipmap_level_;
}
