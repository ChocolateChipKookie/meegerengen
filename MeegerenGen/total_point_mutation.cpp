﻿#include "total_point_mutation.h"
#include "random_engine.h"

total_point_mutation::total_point_mutation(int mutation_chance, std::pair<float, float> point_range_horizontal, std::pair<float, float> point_range_vertical) :
	mutation_chance_(mutation_chance - 1),
	point_range_horizontal_(point_range_horizontal),
	point_range_vertical_(point_range_vertical)
{
}

bool total_point_mutation::mutate(concave_polygon_solution* sol)
{
	bool changed = false;

	for (auto& polygon : sol->polygons)
	{
		for (unsigned i = 0; i < polygon.vertices.size(); ++i)
		{
			auto& point = polygon.vertices[i];

			if (kki::random::rng.get_random(mutation_chance_) == 0)
			{
				point.first += kki::random::rng.get_random_float(point_range_horizontal_.first, point_range_horizontal_.second);
				point.second += kki::random::rng.get_random_float(-point_range_vertical_.first, point_range_vertical_.second);
				changed = true;
			}
		}
	}
	return changed;
}
