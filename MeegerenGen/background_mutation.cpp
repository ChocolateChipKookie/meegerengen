﻿#include "background_mutation.h"
#include "random_engine.h"

background_mutation::background_mutation(int mutation_chance, bool colored, double separation_factor, float sigma,
                                         const std::pair<float, float>& color_range): mutation_chance_(mutation_chance),
                                                                                      colored_(colored),
                                                                                      separation_factor_( static_cast<int>(static_cast<double>(precision) * separation_factor)),
                                                                                      sigma_(sigma),
                                                                                      color_range_(color_range)
{
}

bool background_mutation::mutate(concave_polygon_solution* sol)
{
	bool mutated = false;

	auto& color = sol->background;
	if (colored_)
	{
		if (kki::random::rng.get_random(mutation_chance_) == 0)
		{
			for (unsigned i = 0; i < 3; ++i)
			{
				if (kki::random::rng.get_random(precision) < separation_factor_)
				{
					color[i] += kki::random::rng.get_random_float(-sigma_, sigma_);
					if (color[i] < color_range_.first) color[i] = color_range_.first;
					else  if (color[i] > color_range_.second) color[i] = color_range_.second;
					mutated = true;
				}
			}
		}
	}
	else
	{
		if (kki::random::rng.get_random_double() < mutation_chance_)
		{
			if (kki::random::rng.get_random(precision) < separation_factor_)
			{
				float newGrayscale = color[0] + kki::random::rng.get_random_float(-sigma_, sigma_);
				if (newGrayscale < color_range_.first) newGrayscale = color_range_.first;
				else  if (newGrayscale > color_range_.second) newGrayscale = color_range_.second;
				for (unsigned i = 0; i < 3; ++i)
				{
					color[i] = newGrayscale;
				}
			}
		}
	}

	return mutated;
}
