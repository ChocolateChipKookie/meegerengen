﻿#pragma once
#include <string>

namespace meg
{
	class shader
	{
	private:
		unsigned int ID;

		static void check_compile_errors(unsigned int shader, std::string type);

	public:
		shader(const char* vertexPath, const char* fragmentPath);
		~shader();

		unsigned get_id() const;

		// activate the shader
		void use() const;

		// utility uniform functions
		void set_bool(const std::string& name, bool value) const;
		void set_int(const std::string& name, int value) const;
		void set1f(const std::string& name, float value) const;
		void set2f(const std::string& name, float x, float y) const;
		void set3f(const std::string& name, float x, float y, float z) const;
		void set4f(const std::string& name, float x, float y, float z, float w) const;
	};
}

