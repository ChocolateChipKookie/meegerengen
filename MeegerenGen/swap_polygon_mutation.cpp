﻿#include "swap_polygon_mutation.h"
#include "random_engine.h"

swap_polygon_mutation::swap_polygon_mutation(int mutation_chance): mutation_chance_(mutation_chance - 1)
{
}

bool swap_polygon_mutation::mutate(concave_polygon_solution* sol)
{
	auto& poly = sol->polygons;
	if (sol->polygons.empty() || sol->polygons.size() == 1) return false;
	if (kki::random::rng.get_random(mutation_chance_) == 0)
	{
		const unsigned first = kki::random::rng.get_random(static_cast<int>(poly.size() - 1));
		const unsigned second = kki::random::rng.get_random(static_cast<int>(poly.size() - 1));
		const auto tmp = poly[first];
		poly.erase(poly.begin() + first);
		poly.insert(poly.begin() + second, tmp);
		return true;
	}
	return false;
}
