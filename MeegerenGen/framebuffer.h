﻿#pragma once
#include "texture.h"

namespace meg
{
	class framebuffer
	{
		unsigned id{ 0 };
		texture* texture_;
		unsigned int rbo_;

	public:
		framebuffer(texture* texture);
		~framebuffer();

		void clear();
		void use();
		void end();

		void save_to_file(std::string name);

		unsigned get_id() const;
		texture* get_texture() const;
	};
}
