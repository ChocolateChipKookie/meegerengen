﻿#pragma once

class visualizer
{
public:
	virtual void init() = 0;
	virtual void start() = 0;
	virtual void close() = 0;

	virtual ~visualizer() = default;
};
