﻿#include "concave_polygon_solution.h"

void concave_polygon_solution::draw(meg::drawer& drawer)
{
	background[3] = 1.f;
	drawer.clear(background);
	for (auto& polygon : polygons)
	{
		drawer.concave_poly(polygon.vertices, polygon.color);
	}
}

concave_polygon_solution* concave_polygon_solution::clone()
{
	return new concave_polygon_solution(*this);
}