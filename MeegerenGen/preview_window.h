#pragma once
#include <string>
#include "window_context.h"
#include "texture.h"
#include "opengl_drawer.h"

class preview_window
{
public:
	static void preview(std::string path)
	{
		meg::window window(10, 10, "Preview window!");
		meg::texture t(path);
		window.resize(t.get_width(), t.get_height());
		meg::opengl_drawer drawer;

		while (!window.should_close())
		{
			drawer.texture(t);
			window.swap_buffers();
			window.poll_events();
		}
	}
};
