﻿#include "color_mutation.h"
#include <utility>
#include "random_engine.h"

color_mutation::color_mutation(int mutation_chance, bool colored, double separation_factor, double alpha_factor,
                               float sigma, float alpha_sigma, std::pair<float, float> alpha_range,
                               std::pair<float, float> color_range): mutation_chance_(mutation_chance - 1),
                                                                            colored_(colored),
                                                                            separation_factor_(static_cast<int>(separation_factor * precision)),
                                                                            alpha_factor_(static_cast<int>(alpha_factor * precision)),
                                                                            sigma_(sigma),
                                                                            alpha_sigma_(alpha_sigma),
                                                                            alpha_range_(std::move(alpha_range)),
                                                                            color_range_(std::move(color_range))
{
}

bool color_mutation::mutate(concave_polygon_solution* sol)
{
	bool mutated = false;

	for(auto& poly : sol->polygons)
	{
		auto& color = poly.color;
		if (colored_)
		{
			if (kki::random::rng.get_random(mutation_chance_) == 0)
			{
				for (unsigned i = 0; i < 3; ++i)
				{
					if (kki::random::rng.get_random(precision) < separation_factor_)
					{
						color[i] += kki::random::rng.get_random_float(-sigma_, sigma_);
						if (color[i] < color_range_.first) color[i] = color_range_.first;
						else  if (color[i] > color_range_.second) color[i] = color_range_.second;
						mutated = true;
					}
				}
				if (kki::random::rng.get_random(precision) < alpha_factor_)
				{
					color[3] += kki::random::rng.get_random_float(-alpha_sigma_, alpha_sigma_);
					if (color[3] < alpha_range_.first) color[3] = alpha_range_.first;
					else  if (color[3] > alpha_range_.second) color[3] = alpha_range_.second;
					mutated = true;
				}
			}
		}
		else
		{
			if (kki::random::rng.get_random_double() < mutation_chance_)
			{
				if (kki::random::rng.get_random(precision) < separation_factor_)
				{
					float newGrayscale = color[0] + kki::random::rng.get_random_float(-sigma_, sigma_);
					if (newGrayscale < color_range_.first) newGrayscale = color_range_.first;
					else  if (newGrayscale > color_range_.second) newGrayscale = color_range_.second;
					for (unsigned i = 0; i < 3; ++i)
					{
						color[i] = newGrayscale;
					}
				}
				if (kki::random::rng.get_random(precision) < alpha_factor_)
				{
					color[3] += kki::random::rng.get_random_float(-alpha_sigma_, alpha_sigma_);
					if (color[3] < alpha_range_.first) color[3] = alpha_range_.first;
					else  if (color[3] > alpha_range_.second) color[3] = alpha_range_.second;
					mutated = true;
				}
			}
		}
	}

	return mutated;
}
