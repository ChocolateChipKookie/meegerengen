﻿#include "curving_vertex_mutation.h"
#include "random_engine.h"

curving_vertex_mutation::
curving_vertex_mutation(int mutation_chance, float delete_condition, float intensity) :
	mutation_chance_(mutation_chance), delete_condition_(delete_condition), intensity_(intensity)
{
}

bool curving_vertex_mutation::mutate(concave_polygon_solution* sol)
{
	bool changed = false;

	for (auto& polygon : sol->polygons)
	{
		for (unsigned i = 0; i < polygon.vertices.size(); ++i)
		{
			size_t s = polygon.vertices.size();
			auto& point0 = polygon.vertices[i];
			auto& point1 = polygon.vertices[(i - 1 + s) % s];
			auto& point2 = polygon.vertices[(i + 1) % s];

			if (kki::random::rng.get_random(mutation_chance_) == 0)
			{
				std::pair<float, float> vec_to_c{
					((point0.first + point1.first + point2.first) / 3.f - point0.first) * 2.f,
					((point0.second + point1.second + point2.second) / 3.f - point0.second) * 2.f
				};

				float intensity = kki::random::rng.get_random_float(0, intensity_);
				point0.first = point0.first + vec_to_c.first * intensity;
				point0.second = point0.second + vec_to_c.second * intensity;
				changed = true;

				const float dy = point2.second - point1.second;
				const float dx = point2.first - point1.first;

				const float dist = abs(
						(point2.second - point1.second) * point0.first - (point2.first - point1.first) * point0.second +
						point2.first * point1.second - point2.second * point1.first) / sqrt(dx*dx + dy*dy);

				if(dist < delete_condition_)
				{
					polygon.vertices.erase(polygon.vertices.begin() + i);
				}
			}
		}
	}
	return changed;

}
