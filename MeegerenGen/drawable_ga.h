﻿#pragma once
#include "genetic_algorithm.h"
#include "drawable_solution.h"
#include "mipmap_evaluator.h"
#include "mutation.h"
#include <mutex>

template<typename sol_t>
class drawable_ga : public genetic_algorithm<sol_t>
{
	mutation<sol_t>& mutation_;
	mipmap_evaluator<sol_t>& evaluator_;
	
	std::vector<sol_t*> population_;
	
	std::mutex best_solution_mutex_;
	sol_t* best_solution_{};

	unsigned best_preserved_;
	unsigned not_mutated_;
	unsigned population_size_;

public:


	drawable_ga(mutation<sol_t>& mutation, mipmap_evaluator<sol_t>& evaluator,
		const std::vector<sol_t*>& drawable_solutions, unsigned best_preserved, unsigned not_mutated,
		unsigned population_size)
		: mutation_(mutation),
		  evaluator_(evaluator),
		  population_(drawable_solutions),
		  best_preserved_(best_preserved),
		  not_mutated_(not_mutated),
		  population_size_(population_size)
	{
		evaluator_.evaluate_generation(population_);

		{
			std::unique_lock<std::mutex> get_solution_lock(best_solution_mutex_);
			best_solution_ = (*std::max_element(population_.begin(), population_.end(), [](solution* s1, solution* s2)
				{
					return s1->fitness < s2->fitness;
				}))->clone();
		}
	}

	std::mutex& get_solution_mutex()
	{
		return best_solution_mutex_;
	}

	void reevaluate_population()
	{
		for (auto s : population_) s->evaluated = false;
		evaluator_.evaluate_generation(population_);
	}

	sol_t* get_solution() override
	{
		return best_solution_;
	}

	void run_iteration() override
	{
		for (unsigned k = 0; k < best_preserved_; ++k)
		{
			delete population_[population_size_ - 1 - k];
			population_[population_size_ - 1 - k] = population_[k]->clone();
		}

		for (unsigned k = not_mutated_; k < population_size_; ++k)
		{
			mutation_.mutate(population_[k]);
			population_[k]->evaluated = false;
			evaluator_.evaluate(population_[k]);
		}

		std::sort(population_.begin(), population_.end(), [](drawable_solution* s1, drawable_solution* s2) {return s1->fitness > s2->fitness; });

		if (best_solution_->fitness < population_[0]->fitness)
		{
			std::unique_lock<std::mutex> tmpLock(best_solution_mutex_);
			delete best_solution_;
			best_solution_ = population_[0]->clone();
		}
		++this->iteration_;
	}
};

