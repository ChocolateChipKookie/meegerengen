﻿#include "offscreen_context.h"
#include <stdexcept>
#include "graphics_error.h"

meg::offscreen_context::offscreen_context(bool alpha)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

	GLFWwindow* window = glfwCreateWindow(100, 100, "", nullptr, nullptr);
	if (window == nullptr)
	{
		glfwTerminate();
		throw graphics_error{ "Failed to create GLFW window" };
	}

	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress)))
	{
		throw graphics_error{ "Failed to create GLFW window" };
	}

	if (alpha)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	this->context_ = window;
}

void meg::offscreen_context::use()
{
}
