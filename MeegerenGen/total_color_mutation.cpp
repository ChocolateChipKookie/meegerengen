﻿#include "total_color_mutation.h"
#include "random_engine.h"

total_color_mutation::total_color_mutation(int mutation_chance, bool colored, double separation_factor,
                                           double alpha_factor, std::pair<float, float> alpha_range,
                                           std::pair<float, float> color_range): mutation_chance_(mutation_chance - 1),
                                                                                 colored_(colored),
                                                                                 separation_factor_(static_cast<int>(separation_factor* precision)),
                                                                                 alpha_factor_(static_cast<int>(alpha_factor* precision)),
                                                                                 alpha_range_(std::move(alpha_range)),
                                                                                 color_range_(std::move(color_range))
{
}

bool total_color_mutation::mutate(concave_polygon_solution* sol)
{
	bool mutated = false;

	for(auto& poly : sol->polygons)
	{
		auto& color = poly.color;

		if (colored_)
		{
			if (kki::random::rng.get_random(mutation_chance_) == 0)
			{
				for (unsigned i = 0; i < 3; ++i)
				{
					if (kki::random::rng.get_random(precision) < separation_factor_)
					{
						color[i] = kki::random::rng.get_random_float(color_range_.first, color_range_.second);
						mutated = true;
					}
				}
				if (kki::random::rng.get_random(precision) < alpha_factor_)
				{
					color[3] = kki::random::rng.get_random_float(alpha_range_.first, alpha_range_.second);
					mutated = true;
				}
			}
		}
		else
		{
			if (kki::random::rng.get_random(mutation_chance_) == 0)
			{
				if (kki::random::rng.get_random(precision) < separation_factor_)
				{
					const float new_grayscale = kki::random::rng.get_random_float(color_range_.first, color_range_.second);
					for (unsigned i = 0; i < 3; ++i)
					{
						color[i] = new_grayscale;
						mutated = true;
					}
				}
				if (kki::random::rng.get_random(precision) < alpha_factor_)
				{
					color[3] = kki::random::rng.get_random_float(alpha_range_.first, alpha_range_.second);
					mutated = true;
				}
			}
		}
	}
	
	return mutated;
}
