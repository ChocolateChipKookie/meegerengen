#pragma once

namespace  meg
{
	class context
	{
	public:
		virtual void use() = 0;
		virtual ~context() = default;
	};
}
