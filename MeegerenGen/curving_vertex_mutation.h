﻿#pragma once
#include "concave_polygon_mutation.h"

class curving_vertex_mutation : public concave_polygon_mutation
{
	int mutation_chance_;
	float delete_condition_;
	float intensity_;

public:
	curving_vertex_mutation(int mutation_chance, float delete_condition, float intensity);

	bool mutate(concave_polygon_solution* sol) override;
};
