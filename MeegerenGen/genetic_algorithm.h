#pragma once
#include <type_traits>
#include "solution.h"

template <typename sol_t>
class genetic_algorithm
{
	static_assert(std::is_base_of<solution, sol_t>::value, "sol_t must be a have solution as base");
protected:
	unsigned iteration_ = 0;
	
public:

	class genetic_algorithm_condition
	{
	public:
		virtual ~genetic_algorithm_condition() = default;
		virtual void init(genetic_algorithm* gen_alg) {}
		virtual void update(genetic_algorithm* gen_alg) = 0;
		virtual operator bool() = 0;
	};

	virtual ~genetic_algorithm() = default;
	
	//Returns best solution
	virtual sol_t* get_solution() = 0;
	virtual unsigned get_iteration() { return iteration_; }

	//Runs one iteration of the genetic algorithm
	virtual void run_iteration() = 0;
	//Runs n iterations of the genetic algorithm
	virtual void run_iterations(unsigned iterations)
	{
		for (unsigned i = 0; i < iterations; ++i)
		{
			run_iteration();
		}
	}
	//Runs for loop until condition is satisfied
	virtual void run_condition(genetic_algorithm_condition& con)
	{
		for (con.init(this); con; con.update(this))
		{
			run_iteration();
		}
	}
};

template<typename sol_t>
using condition = typename genetic_algorithm<sol_t>::genetic_algorithm_condition;
