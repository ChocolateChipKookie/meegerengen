#include "meegerengengui.h"
#include <QGridLayout>
#include <QSpinBox>
#include <QSlider>
#include <QDial>
#include <QProgressBar>

MeegerenGenGUI::MeegerenGenGUI(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
}

void MeegerenGenGUI::init()
{
	create_options_group_box();
	create_button_box();

	main_layout = new QGridLayout;
	main_layout->addWidget(rotatable_group_box);
	main_layout->addWidget(options_group_box);
	main_layout->addWidget(button_box_);
	setLayout(main_layout);

	main_layout->setSizeConstraint(QLayout::SetMinimumSize);

	setWindowTitle(tr("Dynamic Layouts"));
}

void MeegerenGenGUI::create_rotatable_group_box()
{
	rotatable_group_box = new QGroupBox(tr("Rotatable Widgets"));

	rotatable_widgets_.enqueue(new QSpinBox);
	rotatable_widgets_.enqueue(new QSlider);
	rotatable_widgets_.enqueue(new QDial);
	rotatable_widgets_.enqueue(new QProgressBar);

	int n = rotatable_widgets_.count();
	for (int i = 0; i < n; ++i) {
		connect(rotatable_widgets_[i], SIGNAL(valueChanged(int)),
			rotatable_widgets_[(i + 1) % n], SLOT(setValue(int)));
	}

	rotatable_layout_= new QGridLayout;
	rotatable_group_box->setLayout(rotatable_layout_);

	rotate_widgets();
}

void MeegerenGenGUI::rotate_widgets()
{
	Q_ASSERT(rotatable_widgets_.count() % 2 == 0);

	for (QWidget* widget : qAsConst(rotatable_widgets_))
		rotatable_layout_->removeWidget(widget);

	rotatable_widgets_.enqueue(rotatable_widgets_.dequeue());

	const int n = rotatable_widgets_.count();
	for (int i = 0; i < n / 2; ++i) {
		rotatable_layout_->addWidget(rotatable_widgets_[n - i - 1]);
		rotatable_layout_->addWidget(rotatable_widgets_[i]);
	}
}

void MeegerenGenGUI::create_options_group_box()
{
	options_group_box = new QGroupBox(tr("Options"));

	button_orientation_label_ = new QLabel(tr("Orientation of buttons:"));

	button_orientation_combo_box_ = new QComboBox;
	button_orientation_combo_box_->addItem(tr("Horizontal"), Qt::Horizontal);
	button_orientation_combo_box_->addItem(tr("Vertical"), Qt::Vertical);

	connect(button_orientation_combo_box_,
		QOverload<int>::of(&QComboBox::currentIndexChanged),
		this,
		&Dialog::buttonsOrientationChanged);

	options_layout_= new QGridLayout;
	options_layout_->addWidget(button_orientation_label_);
	options_layout_->addWidget(button_orientation_combo_box_);
	options_group_box->setLayout(options_layout_);
}

void MeegerenGenGUI::create_button_box()
{
	button_box_ = new QDialogButtonBox;

	close_button_ = button_box_->addButton(QDialogButtonBox::Close);
	help_button_= button_box_->addButton(QDialogButtonBox::Help);
	rotate_button_ = button_box_->addButton(tr("Rotate &Widgets"), QDialogButtonBox::ActionRole);

	connect(rotate_button_, QPushButton::clicked, this, &Dialog::close);
	connect(close_button_, &QPushButton::clicked, this, &Dialog::close);
	connect(help_button_, &QPushButton::clicked, this, &Dialog::help);
}
