﻿#pragma once
#include "visualizer.h"

class drawable_solution_genetic_algorithm_visualizer : public visualizer
{
public:
	void init() override;
	void start() override;
	void close() override;
};
