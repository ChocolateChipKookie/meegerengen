﻿#pragma once
#include "mutation.h"
#include <vector>

template<typename  sol_t>
class obligatory_composite_mutation : public mutation<sol_t>
{
	std::vector<mutation<sol_t>*> mutations_;

public:
	int mutations{ 0 };
	int total{ 0 };

	obligatory_composite_mutation(std::vector<mutation<sol_t>*> mutations);

	bool mutate(sol_t* sol) override;
};


template <typename sol_t>
obligatory_composite_mutation<sol_t>::obligatory_composite_mutation(
	std::vector<mutation<sol_t>*> mutations):
	mutations_(mutations)
{
}

template <typename sol_t>
bool obligatory_composite_mutation<sol_t>::mutate(sol_t* sol)
{
	total++;
	bool mutated = false;
	while (!mutated)
	{
		for(auto& mutation : mutations_)
		{
			if(mutation->mutate(sol))
			{
				//mutated = mutated || mutation->mutate(sol);
				mutated = true;
				mutations++;
			}
		}
	}
	return mutated;
}
