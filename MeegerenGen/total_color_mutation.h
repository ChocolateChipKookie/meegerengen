﻿#pragma once
#include <utility>
#include "concave_polygon_mutation.h"

class total_color_mutation : public concave_polygon_mutation
{
	int mutation_chance_;
	bool colored_;
	int separation_factor_;
	int alpha_factor_;

	std::pair<float, float> alpha_range_;
	std::pair<float, float> color_range_;

public:

	total_color_mutation(int mutation_chance, bool colored, double separation_factor, double alpha_factor,
		std::pair<float, float> alpha_range, std::pair<float, float> color_range = { 0.f, 1.f });

	bool mutate(concave_polygon_solution* sol) override;
};
