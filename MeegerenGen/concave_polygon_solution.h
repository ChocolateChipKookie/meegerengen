﻿#pragma once
#include <utility>
#include <vector>
#include <array>
#include "drawable_solution.h"

struct polygon
{
	std::array<float, 4> color;
	std::vector<std::pair<float, float>> vertices;
};

class concave_polygon_solution : public drawable_solution
{
public:
	concave_polygon_solution(unsigned size)
	{
		polygons.reserve(size);
	}

	void draw(meg::drawer& drawer) override;
	concave_polygon_solution* clone() override;

	size_t vertices()
	{
		size_t total = 0;
		for (auto& polygon : polygons)
		{
			total += polygon.vertices.size();
		}
		return total;
	}

	std::array<float, 4> background{ 0.f, 0.f, 0.f, 0.f };
	std::vector<polygon> polygons;
};
