﻿#pragma once
#include "drawer.h"
#include "solution.h"

class drawable_solution : public solution
{
public:
	virtual void draw(meg::drawer& drawer) = 0;
	drawable_solution* clone() override = 0;
};
