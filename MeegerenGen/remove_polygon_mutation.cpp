﻿#include "remove_polygon_mutation.h"
#include "random_engine.h"

remove_polygon_mutation::remove_polygon_mutation(int mutation_chance) :
	mutation_chance_(mutation_chance - 1)
{
}

bool remove_polygon_mutation::mutate(concave_polygon_solution* sol)
{
	auto& poly = sol->polygons;
	if (poly.empty()) return false;
	if (kki::random::rng.get_random(mutation_chance_) == 0)
	{
		const size_t randPos = kki::random::rng.get_random(static_cast<int>(poly.size() - 1));
		poly.erase(poly.begin() + randPos);
		return true;
	}
	return false;
}
