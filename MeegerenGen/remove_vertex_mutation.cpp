#include "remove_vertex_mutation.h"
#include "random_engine.h"

remove_vertex_mutation::remove_vertex_mutation(int mutation_chance):
	mutation_chance_(mutation_chance - 1)
{
}

bool remove_vertex_mutation::mutate(concave_polygon_solution* sol)
{
	bool changed = false;

	for (auto& polygon : sol->polygons)
	{
		auto points = polygon.vertices;
		if (points.size() <= 3) continue;
		if (kki::random::rng.get_random(mutation_chance_) == 0)
		{
			const size_t randPos = kki::random::rng.get_random(static_cast<int>(points.size() - 1));
			points.erase(points.begin() + randPos);
			changed = true;
		}
	}
	return changed;


}
