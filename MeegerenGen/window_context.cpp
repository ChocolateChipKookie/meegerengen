﻿#include "window_context.h"
#include <iostream>
#include "graphics_error.h"

void meg::window_context::framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void meg::window_context::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

meg::window_context::window_context(int width, int height, const std::string& name, bool alpha) {
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_VISIBLE, GLFW_TRUE);

	GLFWwindow* window = glfwCreateWindow(width, height, name.c_str(), nullptr, nullptr);
	if (window == nullptr)
	{
		glfwTerminate();
		throw graphics_error{ "Failed to create GLFW window" };
	}

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetKeyCallback(window, key_callback);

	if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress)))
	{
		throw graphics_error{ "Failed to create GLFW window" };
	}

	glDisable(GL_LIGHTING);
	glDisable(GL_LIGHTING);

	if (alpha)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	this->window_ = window;
}

meg::window_context::~window_context()
{
	glfwDestroyWindow(window_);
}

bool meg::window_context::should_close() const
{
	return glfwWindowShouldClose(window_);
}

void meg::window_context::set_should_close(bool value) const
{
	glfwSetWindowShouldClose(window_, value);
}

void meg::window_context::swap_buffers() const
{
	glfwSwapBuffers(window_);
}

void meg::window_context::poll_events()
{
	glfwPollEvents();
}

void meg::window_context::set_viewport() const
{
	int width, height;
	glfwGetWindowSize(window_, &width, &height);
	glViewport(0, 0, width, height);
}

void meg::window_context::resize(int width, int height) const
{
	glfwSetWindowSize(window_, width, height);
}

unsigned meg::window_context::get_width() const
{
	int width, height;
	glfwGetWindowSize(window_, &width, &height);
	return width;
}

unsigned meg::window_context::get_height() const
{
	int width, height;
	glfwGetWindowSize(window_, &width, &height);
	return height;
}

void meg::window_context::set_process_input(void(*function)(GLFWwindow*, int, int, int, int)) const
{
	glfwSetKeyCallback(window_, function);
}

void meg::window_context::use()
{
	glfwMakeContextCurrent(window_);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
