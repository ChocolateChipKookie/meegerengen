﻿#pragma once
#include <vector>
#include <array>
#include <stdexcept>
#include "texture.h"

namespace meg
{

	class drawer
	{
	public:
		virtual ~drawer() = default;

		virtual void convex_poly(std::vector<std::pair<float, float>>& vertices, std::array<float, 4> color) = 0;
		virtual void concave_poly(std::vector<std::pair<float, float>>& vertices, std::array<float, 4> color) = 0;
		virtual void triangle(std::array<std::pair<float, float>, 3> vertices, std::array<float, 4> color) = 0;
		//Problem je da je u grafickoj sve normalizirano, tako da krug izgleda kao elipsa
		//Treba se dodati faktor gnjecenja za obe strane ako se misli iole normalno moci crtati
		//Kod toga problem nastaje da ce rezultat biti sjeban, nekako se treba i on denormalizirati, moguci stretch na rez?
		virtual void circle(std::pair<float, float> centerPos, float radius, std::array<float, 4> color, float xWarp = 1, float yWarp = 1) = 0;
		virtual void rectangle(std::pair<float, float> pos, std::pair<float, float> dimensions, std::array<float, 4> color) = 0;
		virtual void set_stroke(float width) = 0;
		virtual void line(std::pair<float, float> p1, std::pair<float, float> p2, std::array<float, 4> color) = 0;
		virtual void subtract(meg::texture& t1, meg::texture& t2) = 0;
		virtual void texture(meg::texture& texture) = 0;

		virtual void clear(std::array<float, 4> color) = 0;
	};
}
