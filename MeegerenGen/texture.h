﻿#pragma once
#include <string>
#include <opencv2/opencv.hpp>

namespace meg
{
	class texture
	{
		unsigned texture_{ 0 };
		int width_{ 0 }, height_{ 0 };
		int max_mipmap_level_{ -1 };
		bool in_use_;
	public:
		explicit texture(std::string filepath);
		explicit texture(cv::Mat image);
		texture(int width, int height);
		texture();
		~texture();

		void generate_mip_map() const;
		int get_max_mipmap_level() const;
		unsigned get_id() const;

		void clear();
		void create(int width, int height);
		void use() const;
		void end() const;

		int get_width() const;
		int get_height() const;
	};
}


