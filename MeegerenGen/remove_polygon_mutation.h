﻿#pragma once
#include "concave_polygon_mutation.h"

class remove_polygon_mutation :  public concave_polygon_mutation
{
	int mutation_chance_;
public:
	remove_polygon_mutation(int mutation_chance);

	bool mutate(concave_polygon_solution* sol) override;
};
