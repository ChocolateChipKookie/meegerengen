﻿#include "color_mutation.h"
#include "random_engine.h"

bool color_mutation::mutate(concave_polygon_solution* sol)
{
	bool mutated = false;

	for(auto& poly : sol->polygons)
	{
		auto& color = poly.color;

		if (colored_)
		{
			if (kki::random::rng.get_random_double() < mutation_chance_)
			{
				for (unsigned i = 0; i < 3; ++i)
				{
					if (kki::random::rng.get_random_double() < separation_factor_)
					{
						color[i] += kki::random::rng.get_random_float(-sigma_, sigma_);
						if (color[i] < color_range_.first) color[i] = color_range_.first;
						else  if (color[i] > color_range_.second) color[i] = color_range_.second;
						mutated = true;
					}
				}
				if (kki::random::rng.get_random_double() < alpha_factor_)
				{
					color[3] += kki::random::rng.get_random_float(-alpha_sigma_, alpha_sigma_);
					if (color[3] < alpha_range_.first) color[3] = alpha_range_.first;
					else  if (color[3] > alpha_range_.second) color[3] = alpha_range_.second;
					mutated = true;
				}
			}
		}
		else
		{
			if (kki::random::rng.get_random_double() < mutation_chance_)
			{
				if (kki::random::rng.get_random_double() < separation_factor_)
				{
					float newGrayscale = color[0] + kki::random::rng.get_random_float(-sigma_, sigma_);
					if (newGrayscale < color_range_.first) newGrayscale = color_range_.first;
					else  if (newGrayscale > color_range_.second) newGrayscale = color_range_.second;
					for (unsigned i = 0; i < 3; ++i)
					{
						color[i] = newGrayscale;
					}
				}
				if (kki::random::rng.get_random_double() < alpha_factor_)
				{
					color[3] += kki::random::rng.get_random_float(-alpha_sigma_, alpha_sigma_);
					if (color[3] < alpha_range_.first) color[3] = alpha_range_.first;
					else  if (color[3] > alpha_range_.second) color[3] = alpha_range_.second;
					mutated = true;
				}
			}
		}

	}
	



	return mutated;

}
