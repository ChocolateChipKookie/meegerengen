﻿#pragma once
#include "drawer.h"
#include "texture.h"
#include <vector>
#include <array>
#include "shader.h"

namespace meg
{

	class opengl_drawer : public drawer
	{
	private:
		//Convex poly
		//Uses triangle fan and stencil buffer
		unsigned int convexPolyVertices{ 4 };
		unsigned int convexPolyVBO{ 0 }, convexPolyVAO{ 0 };
		//Concave poly
		//Uses triangle fan and normal triangle shader
		unsigned int concavePolyVertices{ 4 };
		unsigned int concavePolyVBO{ 0 }, concavePolyVAO{ 0 };
		//Triangle
		shader triangleShader;
		unsigned int triangleVBO{ 0 }, triangleVAO{ 0 };
		//Circle
		unsigned circlePrecision{ 60 };
		std::vector<std::pair<float, float>> circleDirectionPairs;
		//Rectangle
		//Uses triangle fan with 4 points
		unsigned int rectangleVBO{ 0 }, rectangleVAO{ 0 };
		float rectangle_vertices[8]{};
		//Subtract
		shader subtractShader;
		unsigned int subtractVBO{ 0 }, subtractVAO{ 0 }, subtractEBO{ 0 };
		//Texture
		shader textureShader;
		unsigned int textureVBO{ 0 }, textureVAO{ 0 }, textureEBO{ 0 };
		
	public:
		opengl_drawer();
		~opengl_drawer();

		void convex_poly(std::vector<std::pair<float, float>>& vertices, std::array<float, 4> color) override;
		void concave_poly(std::vector<std::pair<float, float>>& vertices, std::array<float, 4> color) override;
		void triangle(std::array<std::pair<float, float>, 3> vertices, std::array<float, 4> color) override;
		//Problem je da je u grafickoj sve normalizirano, tako da krug izgleda kao elipsa
		//Treba se dodati faktor gnjecenja za obe strane ako se misli iole normalno moci crtati
		//Kod toga problem nastaje da ce rezultat biti sjeban, nekako se treba i on denormalizirati, moguci stretch na rez?
		void circle(std::pair<float, float> centerPos, float radius, std::array<float, 4> color, float xWarp = 1, float yWarp = 1) override {throw std::logic_error{"Not implemented!"};}
		void rectangle(std::pair<float, float> pos, std::pair<float, float> dimensions, std::array<float, 4> color) override;
		void set_stroke(float width) override;
		void line(std::pair<float, float> p1, std::pair<float, float> p2, std::array<float, 4> color) override;
		void subtract(meg::texture& t1, meg::texture& t2) override;
		void texture(meg::texture& texture) override;

		static void set_viewport(unsigned width, unsigned height);
		void clear(std::array<float, 4> color) override;
		void set_circle_precision(unsigned triangles);
	};
}
