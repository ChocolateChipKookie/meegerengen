#pragma once
#include "ui_meegerengengui.h"
#include <QGroupBox>
#include <QDialogButtonBox>
#include <QQueue>
#include <QLabel>
#include <QComboBox>

class MeegerenGenGUI : public QMainWindow
{
	Q_OBJECT

public:
	MeegerenGenGUI(QWidget *parent = Q_NULLPTR);

	void init();
	void create_rotatable_group_box();
	void create_options_group_box();
	void create_button_box();
	void rotate_widgets();

private:
	Ui::MeegerenGenGUIClass ui;
	QLayout* main_layout;
	QLayout* rotatable_layout_;
	QLayout* options_layout_;
	QGroupBox* rotatable_group_box;
	QGroupBox* options_group_box;
	QDialogButtonBox* button_box_;

	QQueue<QWidget*> rotatable_widgets_;

	QPushButton* close_button_;
	QPushButton* help_button_;
	QPushButton* rotate_button_;

	QLabel* button_orientation_label_;
	QComboBox* button_orientation_combo_box_;


};
