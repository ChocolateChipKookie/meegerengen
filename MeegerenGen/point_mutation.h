﻿#pragma once
#include "concave_polygon_mutation.h"

class point_mutation : public concave_polygon_mutation
{
	int mutation_chance_;
	int separation_factor_;
	float mutation_intensity_;

	std::pair<float, float> point_range_horizontal_;
	std::pair<float, float> point_range_vertical_;

public:
	point_mutation(int mutation_chance, double separation_factor, float mutation_intensity, std::pair<float, float> point_range_horizontal = { -1.f, 1.f }, std::pair<float, float> point_range_vertical = { -1.f, 1.f });

	bool mutate(concave_polygon_solution* sol) override;
};
