#pragma once
#include <vector>

class solution;

template<typename sol_t>
class crossover
{
	static_assert(std::is_base_of<solution, sol_t>::value, "sol_t must derive from solution");

public:
	virtual ~crossover() = default;

	//Two parents crossover, returns child
	virtual sol_t* cross(sol_t* p1, sol_t* p2) = 0;
	//Optional crossover, gets a vector of parents, and returns a vector of children
	virtual std::vector<sol_t*> cross(std::vector<sol_t*> parents) { return std::vector<sol_t*>(); }
};
