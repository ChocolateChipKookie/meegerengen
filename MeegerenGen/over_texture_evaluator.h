#pragma once
#include "evaluator.h"
#include "framebuffer.h"
#include "opengl_drawer.h"
#include <glad/glad.h>

template<typename sol_t>
class over_texture_evaluator : public evaluator<sol_t>
{
private:
	meg::opengl_drawer& drawer_;
	void init();

	meg::texture& goal_image_;
	meg::texture canvas_texture_;
	meg::framebuffer canvas_framebuffer_;
	meg::texture sub_texture_;
	meg::framebuffer sub_framebuffer_;
	unsigned width_, height_, total_;

	unsigned mipmap_level_;
	int mipmap_level_width_{ 0 };
	int mipmap_level_height_{ 0 };
	std::vector<float> data;

public:
	over_texture_evaluator(meg::texture& goal_image, meg::opengl_drawer& drawer, int width, int height, int mipmap_level);
	~over_texture_evaluator();

	void resize(int width, int height);

	void set_mipmap_level(unsigned mipmap_level);

	void evaluate(sol_t* sol) override;
};

template <typename sol_t>
void over_texture_evaluator<sol_t>::init()
{
	sub_texture_.use();
	glGenerateMipmap(GL_TEXTURE_2D);

	glGetTexLevelParameteriv(GL_TEXTURE_2D, mipmap_level_, GL_TEXTURE_WIDTH, &mipmap_level_width_);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, mipmap_level_, GL_TEXTURE_HEIGHT, &mipmap_level_height_);
	total_ = mipmap_level_width_ * mipmap_level_height_;
	data.resize(4 * total_);
}

template <typename sol_t>
over_texture_evaluator<sol_t>::over_texture_evaluator(meg::texture& goal_image, meg::opengl_drawer& drawer, int width, int height,
	int mipmap_level) :
	drawer_(drawer),
	goal_image_(goal_image),
	canvas_texture_(width, height),
	canvas_framebuffer_(&canvas_texture_),
	sub_texture_(width, height),
	sub_framebuffer_(&sub_texture_),
	width_(width),
	height_(height),
	total_(width* height),
	mipmap_level_(mipmap_level)
{
	init();
}

template <typename sol_t>
over_texture_evaluator<sol_t>::~over_texture_evaluator() = default;

template <typename sol_t>
void over_texture_evaluator<sol_t>::resize(int width, int height)
{
	canvas_texture_.clear();
	canvas_texture_.create(width, height);
	sub_texture_.clear();
	sub_texture_.create(width, height);
	this->width_ = width;
	this->height_ = height;
	this->total_ = width * height;
	data.resize(total_ * 4);
}

template <typename sol_t>
void over_texture_evaluator<sol_t>::set_mipmap_level(unsigned mipmap_level)
{
	mipmap_level_ = mipmap_level;
	init();
}

template <typename sol_t>
void over_texture_evaluator<sol_t>::evaluate(sol_t* sol)
{
	if (sol->evaluated) return;

	canvas_framebuffer_.use();
	drawer_.clear({ 0.f, 0.f, 0.f, 1.f });
	drawer_.set_viewport(width_, height_);

	sol->draw(drawer_);
	//canvasFramebuffer_.end(); //Nepotrebno jer se odmah nakon njegas setta subtract frambuffer

	sub_framebuffer_.use();
	drawer_.clear({ 0.f, 0.f, 0.f, 1.f });
	drawer_.subtract(canvas_texture_, goal_image_);
	sub_framebuffer_.end();

	sub_texture_.use();
	if (mipmap_level_ != 0)
		glGenerateMipmap(GL_TEXTURE_2D);

	glGetTexImage(GL_TEXTURE_2D, mipmap_level_, GL_RGBA, GL_FLOAT, data.data());
	sub_texture_.end();

	double histogram{ 0 };
	for (unsigned i = 0; i < total_; ++i)
	{
		histogram += data[i * 4 + 0] + data[i * 4 + 1] + data[i * 4 + 2];
	}

	sol->value = histogram / static_cast<double>(this->total_) / 3.;
	sol->fitness = -sol->value;
	sol->evaluated = true;
}
