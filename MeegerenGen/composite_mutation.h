#pragma once
#include "mutation.h"
#include <memory>
#include <vector>
#include "random_engine.h"

template<typename sol_t>
class composite_mutation : public mutation<sol_t>
{
	std::vector<std::pair<mutation<sol_t>*, double>> mutations_;
	kki::random_engine& rand = kki::random::rng;

public:
	composite_mutation(std::vector<std::pair<mutation<sol_t>*, double>> mutations);

	bool mutate(sol_t* sol) override;
};

template <typename sol_t>
composite_mutation<sol_t>::composite_mutation(std::vector<std::pair<mutation<sol_t>*, double>> mutations) : mutations_(mutations)
{
	//Normalising the chances
	double total = 0;
	for(auto& mut : mutations_)
	{
		total += mut.second;
	}
	
	for(auto& mut : mutations_)
	{
		mut.second /= total;
	}
}

template <typename sol_t>
bool composite_mutation<sol_t>::mutate(sol_t* sol)
{
	double pos = rand.get_random_double();
	auto it = mutations_.begin();

	while(it < mutations_.end())
	{
		pos -= it->second;
		if(pos <=0 )
			return it->first->mutate(sol);
		++it;
	}
	return mutations_[rand.get_random(mutations_.size() - 1)].first->mutate(sol);
}
