﻿#include "add_vertex_mutation.h"
#include <utility>
#include "random_engine.h"

add_vertex_mutation::add_vertex_mutation(int mutation_chance, unsigned max_vertices,
										std::pair<float, float> interpolation_range, float sigma, 
										std::pair<float, float> point_range_horizontal, std::pair<float, float> point_range_vertical):
	mutation_chance_(mutation_chance - 1),
	max_vertices_(max_vertices),
	interpolation_range_(std::move(interpolation_range)),
	sigma_(sigma),
	point_range_horizontal_(std::move(point_range_horizontal)),
	point_range_vertical_(std::move(point_range_vertical))
{
}

bool add_vertex_mutation::mutate(concave_polygon_solution* sol)
{
	bool changed{ false };

	for(auto& polygon : sol->polygons)
	{
		auto& points = polygon.vertices;
		if (polygon.vertices.size() >= max_vertices_) continue;
		if (kki::random::rng.get_random(mutation_chance_) == 0)
		{
			//The position of the new vertex is going to be between these two vertices
			const size_t vertex1 = kki::random::rng.get_random(static_cast<int>(points.size() - 1));
			const size_t vertex2 = (vertex1 + 1) % points.size();

			//The position of the new point is interpolated between the two old points
			const float position = kki::random::rng.get_random_float(interpolation_range_.first, interpolation_range_.second);
			std::pair<float, float> newPos{
				points[vertex1].first + (points[vertex2].first - points[vertex1].first) * position,
				points[vertex1].second + (points[vertex2].second - points[vertex1].second) * position
			};

			//Move the new point a bit
			newPos.first += kki::random::rng.get_random_float(-sigma_, sigma_);
			newPos.second += kki::random::rng.get_random_float(-sigma_, sigma_);

			/*Possible overhead, but this mutation is seldom enough so it is insignificant*/
			if (newPos.first < point_range_horizontal_.first) newPos.first = point_range_horizontal_.first;
			else  if (newPos.first > point_range_horizontal_.second) newPos.first = point_range_horizontal_.second;
			if (newPos.second < point_range_vertical_.first) newPos.second = point_range_vertical_.first;
			else  if (newPos.second > point_range_vertical_.second) newPos.second = point_range_vertical_.second;

			points.insert(points.begin() + vertex2, newPos);
			changed = true;
		}
	}

	return changed;
}
