#pragma once
#include "mutation.h"
#include "concave_polygon_solution.h"

const int precision = 1000000;

/*
 *	Mutation chances for most, if not all the concave polygon mutations are expressed using an integer i. 
 *	Where the true chance of the mutation happening is 1/i because the rand(i) == 0 performs 10 times faster than testing the same using doubles
 *	In the constructors the we decrement the i by one, because the random_int_distribution gives a random element in this range[0, i]
 *	
 *	Furthermore, some subsequent mutations are expressed using an integer in the range [0, 99999]
 *	Where the true chance of the mutation happening is i/ precision because the rand(precision) < i performs better than any double testing
 */
class concave_polygon_mutation : public mutation<concave_polygon_solution>
{
public:
	bool mutate(concave_polygon_solution* sol) override = 0;
};
