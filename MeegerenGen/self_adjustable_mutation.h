﻿#pragma once
#include <vector>
#include "mutation.h"

template<typename sol_t>
class self_adjustable_mutation : public mutation<sol_t>
{
	std::vector<mutation<sol_t>*> mutations_;



public:
	int mutations{ 0 };
	int total{ 0 };
	const int treshold_ = 50;

	self_adjustable_mutation(std::vector<mutation<sol_t>*> mutations, double min_probability, unsigned iterations, unsigned tries);

	static const unsigned precision = 1000000;
	static const unsigned precision_ = precision - 1;
	unsigned counter = 0;
	unsigned iterations = 0;
	
	int min_probability;
	std::vector<unsigned> probabilities_;
	unsigned total_probability_;
	std::vector<unsigned> uses_;
	std::vector<unsigned> hits_;
	std::vector<bool> used_;

	unsigned tries;

	bool mutate(sol_t* sol) override;
	void update(bool good);
};


template <typename sol_t>
self_adjustable_mutation<sol_t>::self_adjustable_mutation(
	std::vector<mutation<sol_t>*> mutations, double min_probability, unsigned iterations, unsigned tries) :
	mutations_(mutations),
	iterations(iterations),
	min_probability(precision* min_probability),
	tries(tries)
{
	uses_.resize(mutations.size());
	hits_.resize(mutations.size());
	used_.resize(mutations.size());
	probabilities_.resize(mutations.size(), this->min_probability);
	total_probability_ = this->min_probability * mutations_.size();
}

template <typename sol_t>
bool self_adjustable_mutation<sol_t>::mutate(sol_t* sol)
{
	total++;
	bool mutated = false;
	
	std::fill(used_.begin(), used_.end(), false);
	
	while (!mutated)
	{
		for(unsigned i = 0; i < tries; ++i)
		{
			int position = kki::random::rng.get_random(total_probability_);

			unsigned p = 0;
			while(true)
			{
				position -= probabilities_[p];
				if(position <= 0)
				{
					break;
				}
				p++;
			}

			if (mutations_[p]->mutate(sol))
			{
				//mutated = mutated || mutation->mutate(sol);
				mutated = true;
				used_[p] = true;
				uses_[p]++;
				mutations++;
			}
		}
	}
	++counter;
	if (counter == iterations)
	{
		counter = 0;
		bool ready = false;

		for(auto use : uses_)
		{
			if(use > treshold_)
			{
				ready = true;
				break;
			}
		}
		if(ready)
		{
			std::fill(probabilities_.begin(), probabilities_.end(), this->min_probability);

			std::vector<double> percentages(mutations_.size());
			double max{ 0 };
			double min{ 1 };
			for (unsigned i = 0; i < mutations_.size(); ++i)
			{
				percentages[i] = static_cast<double>(hits_[i]) / static_cast<double>(uses_[i]);
				if (percentages[i] > max) max = percentages[i];
				else if (percentages[i] < min) min = percentages[i];
			}

			for (auto& p : percentages)
			{
				p -= min;
				p /= max - min;
			}

			total_probability_ = 0;
			for (unsigned i = 0; i < mutations_.size(); ++i)
			{
				probabilities_[i] += static_cast<unsigned>(percentages[i] * precision);
				total_probability_ += probabilities_[i];
			}
			total_probability_--;
			std::fill(uses_.begin(), uses_.end(), 0);
			std::fill(hits_.begin(), hits_.end(), 0);
		}
	}

	return mutated;
	

}

template <typename sol_t>
void self_adjustable_mutation<sol_t>::update(bool good)
{
	if(good)
	{
		for (unsigned i = 0; i < mutations_.size(); ++i)
		{
			if(used_[i])
			{
				hits_[i]++;
			}
		}
	}
}
