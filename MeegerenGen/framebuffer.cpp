﻿#include "framebuffer.h"
#include <iostream>
#include <opencv2/opencv.hpp>
#include <glad/glad.h>

meg::framebuffer::framebuffer(meg::texture* texture) : texture_(texture)
{
	glGenFramebuffers(1, &id);
	glBindFramebuffer(GL_FRAMEBUFFER, id);

	//Attach it to currently bound framebuffer object
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture->get_id(), 0);

	glGenRenderbuffers(1, &rbo_);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo_);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, texture->get_width(), texture->get_height());
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo_);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		assert(true);
		std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << std::endl;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

meg::framebuffer::~framebuffer()
{
	glDeleteFramebuffers(1, &id);
}

void meg::framebuffer::clear()
{
	glDeleteFramebuffers(1, &id);
}

void meg::framebuffer::use()
{
	glBindFramebuffer(GL_FRAMEBUFFER, id);
}

void meg::framebuffer::end()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void meg::framebuffer::save_to_file(std::string name)
{
	unsigned char* pixels = new unsigned char[texture_->get_width() * texture_->get_height() * 3];

	use();
	/// READ THE CONTENT FROM THE FBO
	glReadBuffer(GL_COLOR_ATTACHMENT0);
	glReadPixels(0, 0, texture_->get_width(), texture_->get_height(), GL_RGB, GL_UNSIGNED_BYTE, pixels);
	end();

	const cv::Mat resultMat(texture_->get_width(), texture_->get_height(), CV_8UC3, pixels);
	cv::cvtColor(resultMat, resultMat, CV_BGR2RGB);
	cv::flip(resultMat, resultMat, 0);
	cv::imwrite(name, resultMat);
	delete[]pixels;
}

unsigned meg::framebuffer::get_id() const
{
	return id;
}

meg::texture* meg::framebuffer::get_texture() const
{
	return texture_;
}
