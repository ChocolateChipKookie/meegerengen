﻿#include "point_mutation.h"
#include <utility>
#include "random_engine.h"

point_mutation::point_mutation(int mutation_chance, double separation_factor, float mutation_intensity,
                               std::pair<float, float> point_range_horizontal,
                               std::pair<float, float> point_range_vertical) :
	mutation_chance_(mutation_chance - 1),
	separation_factor_(static_cast<int>(separation_factor * precision)),
	mutation_intensity_(mutation_intensity),
	point_range_horizontal_(std::move(point_range_horizontal)),
	point_range_vertical_(std::move(point_range_vertical))
{
}

bool point_mutation::mutate(concave_polygon_solution* sol)
{
	bool changed = false;

	for(auto& polygon : sol->polygons)
	{
		for(unsigned i = 0; i < polygon.vertices.size(); ++i)
		{
			auto& point = polygon.vertices[i];

			if (kki::random::rng.get_random(mutation_chance_) == 0)
			{
				if (kki::random::rng.get_random(precision) < separation_factor_)
					point.first += kki::random::rng.get_random_float(-mutation_intensity_, mutation_intensity_);
				if (kki::random::rng.get_random(precision) < separation_factor_)
					point.second += kki::random::rng.get_random_float(-mutation_intensity_, mutation_intensity_);

				if (point.first < point_range_horizontal_.first) point.first = point_range_horizontal_.first;
				else  if (point.first > point_range_horizontal_.second) point.first = point_range_horizontal_.second;
				if (point.second < point_range_vertical_.first) point.second = point_range_vertical_.first;
				else  if (point.second > point_range_vertical_.second) point.second = point_range_vertical_.second;
				changed = true;
			}
		}
	}
	return changed;
}
