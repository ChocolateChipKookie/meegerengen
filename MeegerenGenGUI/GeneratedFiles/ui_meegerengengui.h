/********************************************************************************
** Form generated from reading UI file 'meegerengengui.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MEEGERENGENGUI_H
#define UI_MEEGERENGENGUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MeegerenGenGUIClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MeegerenGenGUIClass)
    {
        if (MeegerenGenGUIClass->objectName().isEmpty())
            MeegerenGenGUIClass->setObjectName(QString::fromUtf8("MeegerenGenGUIClass"));
        MeegerenGenGUIClass->resize(600, 400);
        menuBar = new QMenuBar(MeegerenGenGUIClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        MeegerenGenGUIClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MeegerenGenGUIClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MeegerenGenGUIClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(MeegerenGenGUIClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        MeegerenGenGUIClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MeegerenGenGUIClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MeegerenGenGUIClass->setStatusBar(statusBar);

        retranslateUi(MeegerenGenGUIClass);

        QMetaObject::connectSlotsByName(MeegerenGenGUIClass);
    } // setupUi

    void retranslateUi(QMainWindow *MeegerenGenGUIClass)
    {
        MeegerenGenGUIClass->setWindowTitle(QApplication::translate("MeegerenGenGUIClass", "MeegerenGenGUI", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MeegerenGenGUIClass: public Ui_MeegerenGenGUIClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MEEGERENGENGUI_H
