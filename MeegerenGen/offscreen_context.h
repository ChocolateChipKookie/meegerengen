﻿#pragma once
#include "context.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>

namespace meg
{
	class offscreen_context final : public context
	{
	private:
		GLFWwindow* context_;
	public:
		offscreen_context(bool alpha);
		~offscreen_context() = default;
		void use() override;
	};	
}
