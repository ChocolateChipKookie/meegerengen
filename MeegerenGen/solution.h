#pragma once

class solution
{
public:
	virtual solution* clone() = 0;
	virtual ~solution() = default;

	double fitness{};
	double value{};
	bool evaluated{false};
};
