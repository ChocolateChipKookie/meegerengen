#pragma once
#include "solution.h"
#include "evaluator.h"
#include <cmath>
#include <random>
#include "mutation.h"
#include "crossover.h"
#include "general_ga.h"
#include "k_tournament_selection.h"
#include <iostream>

class double_solution : public solution
{
public:
	double_solution(double val) : val(val) {}
	double val{ 0 };
	double_solution* clone() override { return new double_solution(*this); }
};

class double_evaluator : public evaluator<double_solution>
{
public:

	double goal_;

	double_evaluator(double goal) : goal_(goal) {}

	void evaluate(double_solution* sol) override
	{
		sol->value = abs(goal_ - sol->val);
		sol->fitness = -sol->value;
	}
};

class def_mutation : public mutation<double_solution>
{
public:
	std::default_random_engine eng;

	bool mutate(double_solution* sol) override
	{
		sol->val += std::normal_distribution<double>(0, 0.01)(eng);
		return true;
	}
};

class def_crossover : public crossover<double_solution>
{
public:
	double_solution* cross(double_solution* p1, double_solution* p2) override
	{
		double_solution* solu = p1->clone();
		solu->val = (p1->val + p2->val) / 2.;
		return solu;
	}
};

void run_test()
{
	std::vector<double_solution*> start;

	std::default_random_engine random;

	for (unsigned i = 0; i < 50; ++i)
	{
		start.push_back(new double_solution(1000 * std::uniform_real_distribution<double>()(random)));
	}

	double_evaluator de(142);
	def_crossover dc;
	def_mutation dm;
	k_tournament_selection<double_solution> kts(3);

	general_ga<double_solution> ga(start, 50, de, dm, dc, kts);

	for (unsigned i = 1; i <= 100000; ++i)
	{
		std::cout << "Best: " << ga.get_solution()->fitness << std::endl;
		ga.run_iteration();
	}
	std::cout << "Best: " << ga.get_solution()->val << std::endl;
}

bool test_bool(unsigned& i)
{
	if(!(i%1000))
	{
		++i;
	}
	i += 1;
	return true;
}

void test_void(unsigned& i)
{
	if (!(i % 1000))
	{
		++i;
	}
	i += 1;
}