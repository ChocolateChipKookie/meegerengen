#pragma once
#include "concave_polygon_mutation.h"


class add_polygon_mutation : public concave_polygon_mutation
{
private:
	int mutation_chance_;
	unsigned max_polygons_;
	double min_triangle_size_;
	float sigma_;
	std::pair<float, float> alpha_range_;
	bool colored_;

	std::pair<float, float> point_range_horizontal_;
	std::pair<float, float> point_range_vertical_;

	static float triangle_size(polygon& poly);

public:
	add_polygon_mutation(int mutation_chance, unsigned max_polygons, double min_triangle_size, float sigma,
						std::pair<float, float> alpha_range_, bool colored_,
		std::pair<float, float> point_range_horizontal_ = {-1.f, 1.f}, std::pair<float, float> point_range_vertical_ = { -1.f, 1.f });

	bool mutate(concave_polygon_solution* sol) override;
};

