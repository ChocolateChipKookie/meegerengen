﻿#pragma once
#include "genetic_algorithm.h"
#include "evaluator.h"
#include "crossover.h"
#include "mutation.h"
#include "selection.h"
#include <algorithm>

template<typename sol_t>
class general_ga : public genetic_algorithm<sol_t>
{
	static_assert(std::is_base_of<solution, sol_t>::value, "sol_t must be a have solution as base");

private:
	std::vector<sol_t*> population_;
	unsigned population_size_;
	evaluator<sol_t>& eval_;
	mutation<sol_t>& mut_;
	crossover<sol_t>& cross_;
	selection<sol_t>& sel_;

	sol_t* best_solution_{nullptr};


public:

	general_ga(std::vector<sol_t*> starting_population, unsigned population_size, evaluator<sol_t>& eval,
	           mutation<sol_t>& mut, crossover<sol_t>& cross, selection<sol_t>& select);

	sol_t* get_solution() override;
	void run_iteration() override;
};

template <typename sol_t>
general_ga<sol_t>::general_ga(std::vector<sol_t*> starting_population, unsigned population_size, evaluator<sol_t>& eval,
                              mutation<sol_t>& mut, crossover<sol_t>& cross, selection<sol_t>& select):
	population_(std::move(starting_population)),
	population_size_(population_size),
	eval_(eval),
	mut_(mut),
	cross_(cross),
	sel_(select)
{
	eval_.evaluate_generation(starting_population);
	best_solution_ = *std::max_element(population_.begin(), population_.end(), [](solution* s1, solution* s2) {return s1->fitness < s2->fitness; });
}

template <typename sol_t>
sol_t* general_ga<sol_t>::get_solution()
{
	return best_solution_;
}

template <typename sol_t>
void general_ga<sol_t>::run_iteration()
{
	std::vector<sol_t*> new_generation(population_size_, nullptr);

	for (unsigned i = 0; i < population_size_; ++i)
	{
		new_generation[i] = cross_.cross(sel_.select(population_), sel_.select(population_));
		mut_.mutate(new_generation[i]);
	}

	for (auto unit : population_)
	{
		delete unit;
	}

	population_ = new_generation;
	eval_.evaluate_generation(population_);

	best_solution_ = *std::max_element(population_.begin(), population_.end(), [](solution* s1, solution* s2) {return s1->fitness < s2->fitness; });
	new_generation.clear();
	++this->iteration_;
}
