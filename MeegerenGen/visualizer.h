﻿#pragma once

class visualiser
{
public:
	virtual void init() = 0;
	virtual void start() = 0;
	virtual void close() = 0;

	virtual ~visualiser() = default;
};
