#include "add_polygon_mutation.h"
#include <utility>
#include "random_engine.h"

float add_polygon_mutation::triangle_size(polygon& poly)
{
	const float ax = poly.vertices[0].first - poly.vertices[1].first;
	const float ay = poly.vertices[0].second - poly.vertices[1].second;
	const float a = std::sqrt(ax * ax + ay * ay);
	const float bx = poly.vertices[1].first - poly.vertices[2].first;
	const float by = poly.vertices[1].second - poly.vertices[2].second;
	const float b = std::sqrt(bx * bx + by * by);
	const float cx = poly.vertices[2].first - poly.vertices[0].first;
	const float cy = poly.vertices[2].second - poly.vertices[0].second;
	const float c = std::sqrt(cx * cx + cy * cy);

	const float s = a + b + c;

	return std::sqrt(s * (s - a) * (s - b) * (s - c));
}

add_polygon_mutation::add_polygon_mutation(int mutation_chance, unsigned max_polygons, double min_triangle_size,
											float sigma, std::pair<float, float> alpha_range, bool colored_,
											std::pair<float, float> point_range_horizontal,
											std::pair<float, float> point_range_vertical)
	: mutation_chance_(mutation_chance - 1),
	max_polygons_(max_polygons),
	min_triangle_size_(min_triangle_size),
	sigma_(sigma),
	alpha_range_(std::move(alpha_range)),
	colored_(colored_),
	point_range_horizontal_(std::move(point_range_horizontal)),
	point_range_vertical_(std::move(point_range_vertical))
{
}

bool add_polygon_mutation::mutate(concave_polygon_solution* sol)
{
	if (sol->polygons.size() >= max_polygons_) return false;
	if (kki::random::rng.get_random(mutation_chance_) == 0)
	{
		polygon poly;

		kki::random_engine& rand = kki::random::rng;

		const float new_grayscale = rand.get_random_float();
		poly.color = {
			colored_ ? rand.get_random_float() : new_grayscale,
			colored_ ? rand.get_random_float() : new_grayscale,
			colored_ ? rand.get_random_float() : new_grayscale,
			rand.get_random_float(alpha_range_.first, alpha_range_.second)
		};

		while (true)
		{
			poly.vertices.clear();
			const std::pair<float, float> center = {
				rand.get_random_float(point_range_horizontal_.first, point_range_horizontal_.second),
				rand.get_random_float(point_range_vertical_.first, point_range_vertical_.second)
			};

			for (unsigned i = 0; i < 3; ++i)
			{
				float x = center.first + rand.get_random_float(-sigma_, sigma_);
				float y = center.second + rand.get_random_float(-sigma_, sigma_);

				if (x < point_range_horizontal_.first) x = point_range_horizontal_.first;
				else  if (x > point_range_horizontal_.second) x = point_range_horizontal_.second;
				if (y < point_range_vertical_.first) y = point_range_vertical_.first;
				else  if (y > point_range_vertical_.second) y = point_range_vertical_.second;
				poly.vertices.emplace_back(x, y);
			}
			if (triangle_size(poly) > min_triangle_size_)
				break;
		}
		sol->polygons.push_back(poly);

		return true;
	}
	return false;
}
