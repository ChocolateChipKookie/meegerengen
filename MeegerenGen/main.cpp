#include "timer.h"
#include "random_engine.h"
#include "preview_window.h"
#include "drawable_ga.h"
#include "concave_polygon_solution.h"
#include "total_point_mutation.h"
#include "obligatory_composite_mutation.h"
#include "offscreen_context.h"
#include "add_polygon_mutation.h"
#include "remove_polygon_mutation.h"
#include "add_vertex_mutation.h"
#include "color_mutation.h"
#include "point_mutation.h"
#include "remove_vertex_mutation.h"
#include "swap_polygon_mutation.h"
#include "total_color_mutation.h"
#include "background_mutation.h"
#include "curving_vertex_mutation.h"
#include "self_adjustable_mutation.h"
#include "self_adjustable_drawable_ga.h"

void drawer(self_adjustable_drawable_ga<concave_polygon_solution>* drawable, const bool* running)
{
	meg::window win{100, 200, "Off_window", true};
	meg::opengl_drawer drawer;

	double best = 0;
	double best_tmp = 0;
	while(*running)
	{
		win.poll_events();

		{
			std::unique_lock<std::mutex> ul{ drawable->get_solution_mutex() };
			drawable->get_solution()->draw(drawer);
			best_tmp = drawable->get_solution()->fitness;
		}

		if(best != best_tmp)
		{
			std::cout << drawable->get_iteration() << ". " << best_tmp << std::endl;
			best = best_tmp;
		}
		win.swap_buffers();
		std::this_thread::sleep_for(std::chrono::milliseconds{ 100 });
	}
}

int main(int argc, char* argv[])
{
	/*
	meg::window win{ 100, 200, "Off_window", true };
	meg::opengl_drawer drawer;

	concave_polygon_solution cps(100);

	cps.background = { 
		kki::random::rng.get_random_float(),
		kki::random::rng.get_random_float(),
		kki::random::rng.get_random_float(),
		1
	};

	for(unsigned i = 0; i < 100; ++i)
	{
		polygon p;

		p.color = {
		kki::random::rng.get_random_float(),
		kki::random::rng.get_random_float(),
		kki::random::rng.get_random_float(),
		kki::random::rng.get_random_float(0.1, 0.5)
		};
		int vertices = kki::random::rng.get_random(10);

		for(unsigned k = 0; k < vertices;++k)
		{		
			p.vertices.emplace_back(kki::random::rng.get_random_float(-1, 1), kki::random::rng.get_random_float(-1, 1));
		}

		cps.polygons.push_back(p);
	}
	meg::texture image_tex{ "slika.jpg" };
	meg::texture sub_tex{ image_tex.get_width(), image_tex.get_height() };
	meg::texture draw_tex{ 500, 500 };
	
	draw_tex.clear();
	draw_tex.create(1000, 1000);
	sub_tex.clear();
	sub_tex.create(1000, 1000);


	meg::framebuffer image_framebuffer{ &image_tex };
	meg::framebuffer sub_framebuffer{ &sub_tex };
	meg::framebuffer draw_framebuffer{ &draw_tex };


	draw_framebuffer.use();
	drawer.set_viewport(draw_tex.get_width(), draw_tex.get_height());
	cps.draw(drawer);
	draw_tex.generate_mip_map();

	sub_framebuffer.use();
	drawer.set_viewport(sub_tex.get_width(), sub_tex.get_height());
	drawer.subtract(image_tex, draw_tex);
	sub_tex.generate_mip_map();


	win.use();
	drawer.set_viewport(win.get_width(), win.get_height());
	while (true)
	{
		win.poll_events();
		drawer.texture(sub_tex);
		win.swap_buffers();
		std::this_thread::sleep_for(std::chrono::milliseconds{ 30 });
	}

	*/

	
	//OpenGL context
	meg::offscreen_context ofc{ true };
	
	//Constants
	unsigned best_preserved { 2 };
	unsigned not_mutated { 1 };
	unsigned population_size { 5 };
	unsigned vertices { 25 };
	unsigned polygons { 500 };


	std::string src{ "slika.jpg" };

	timer::global()["Total"].start();
	//Initial population
	std::vector<concave_polygon_solution*> drawable_solutions;
	drawable_solutions.reserve(population_size);

	for (unsigned i = 0; i < population_size; ++i) drawable_solutions.push_back(new concave_polygon_solution{ polygons });

	//Evaluator
	meg::texture tex{ src };
	meg::opengl_drawer ogld;

	mipmap_evaluator<concave_polygon_solution> evaluator{ tex, ogld, 256, 256, 1 };

	//Mutations
	std::vector<mutation<concave_polygon_solution>*> mutations;
	{
		//mutations.push_back(new add_polygon_mutation{ 2, polygons, 0.05, 0.25, {0.1f, 0.9f}, true });
		mutations.push_back(new background_mutation{ 250, true, 0.4f, 0.05f, {0.f, 1.f} });
		mutations.push_back(new color_mutation{ 75, true, 0.3, 0.3, 0.05f, 0.02f, {0.1f, 0.6f} });
		mutations.push_back(new curving_vertex_mutation{ 1000, 0.02, 0.25 });
		mutations.push_back(new point_mutation{ 300, 0.8, 0.05f });
		mutations.push_back(new point_mutation{ 500, 0.8, 0.15f });
		mutations.push_back(new add_vertex_mutation{ 175, vertices, {0.3f, 0.7f}, 0.05f });
		mutations.push_back(new remove_vertex_mutation{ 125 });
		mutations.push_back(new add_polygon_mutation{ 150, polygons, 0.005, 0.05, {0.1f, 0.4f}, true });
		mutations.push_back(new remove_polygon_mutation{ 200 });
		mutations.push_back(new swap_polygon_mutation{ 100 });
		//mutations.push_back(new remove_vertex_mutation{ 2 });
	}

	//obligatory_composite_mutation<concave_polygon_solution> ocm{ mutations };
	self_adjustable_mutation<concave_polygon_solution> ocm{ mutations, 0.4, 5000, 15};

	//drawable_ga<concave_polygon_solution> dga{ ocm, evaluator, drawable_solutions, best_preserved, not_mutated, population_size };
	self_adjustable_drawable_ga<concave_polygon_solution> dga{ocm, evaluator, drawable_solutions, best_preserved, not_mutated, population_size};

	bool running = true;
	std::thread drawer_thread{ drawer, &dga, &running };

	evaluator.set_mipmap_level(4);
	dga.reevaluate_population();
	dga.run_iterations(7500);
	evaluator.set_mipmap_level(3);
	dga.reevaluate_population();
	dga.run_iterations(15000);
	evaluator.set_mipmap_level(2);
	dga.reevaluate_population();
	dga.run_iterations(30000);
	evaluator.set_mipmap_level(1);
	dga.reevaluate_population();
	while (true)
	{
		dga.run_iteration();
	}



	running = false;
	drawer_thread.join();

	
	//do 10000 4
	//do 20000 3
	//do 40000 2
	//Ostalo 1

	/*
	concave_polygon_solution cps(100);

	cps.background = {
		kki::random::rng.get_random_float(),
		kki::random::rng.get_random_float(),
		kki::random::rng.get_random_float(),
		1
	};

	for (unsigned k = 0; k < 100; ++k)
	{
		polygon p;

		p.color = {
		kki::random::rng.get_random_float(),
		kki::random::rng.get_random_float(),
		kki::random::rng.get_random_float(),
		kki::random::rng.get_random_float(0.1, 0.5)
		};
		int vertices = kki::random::rng.get_random(10);

		for (unsigned j = 0; j < vertices; ++j)
		{
			p.vertices.emplace_back(kki::random::rng.get_random_float(-1, 1), kki::random::rng.get_random_float(-1, 1));
		}

		cps.polygons.push_back(p);
	}

	for(unsigned k = 0; k < 7; k++)
	{
		evaluator.set_mipmap_level(k);
		cps.evaluated = false;
		evaluator.evaluate(&cps);
		std::cout << "Level " << k << ": " << cps.fitness << std::endl;
	}
	*/

}
