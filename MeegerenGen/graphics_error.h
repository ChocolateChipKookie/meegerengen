#pragma once
#include <stdexcept>


namespace meg
{
	class graphics_error final : public std::runtime_error
	{
	public:
		graphics_error(std::string& message) : runtime_error(message) {};
		graphics_error(const char* message) : runtime_error(message) {};
	};
}
